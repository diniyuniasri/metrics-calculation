package com.tool.calculation.metrics.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ClassIndexModel {

    private Integer startDeclaration;

    private Integer endDeclaration;

    private Integer endClass;
}
