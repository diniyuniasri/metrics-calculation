package com.tool.calculation.metrics.model;

import lombok.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author M. Dini Yuniasri
 * @version 1.0.0
 * @since 27 April 2019
 */

@Data
@Builder(builderMethodName = "blockBuilder")
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class BlockModel extends StatementModel {

    @Builder.Default
    private List<StatementModel> statements = new ArrayList<>();

    private StatementModel endOfBlockStatement;
}
