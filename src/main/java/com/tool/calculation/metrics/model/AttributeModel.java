package com.tool.calculation.metrics.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AttributeModel {
    private String type;

    private String name;

    private String value;
}
