package com.tool.calculation.metrics.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class IndexModel {

    private Integer start;

    private Integer end;
}
