package com.tool.calculation.metrics.service;

import com.tool.calculation.metrics.model.ClassModel;
import com.tool.calculation.metrics.model.FileModel;
import lombok.NonNull;

public interface ClassPackageImportAnalysis {

    void analysis(@NonNull FileModel fileModel, @NonNull ClassModel classModel);
}
