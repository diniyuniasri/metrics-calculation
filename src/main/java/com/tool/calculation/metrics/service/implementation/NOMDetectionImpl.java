package com.tool.calculation.metrics.service.implementation;

import com.tool.calculation.metrics.model.ClassModel;
import com.tool.calculation.metrics.service.NOMDetection;
import lombok.NonNull;
import org.springframework.stereotype.Service;

@Service
public class NOMDetectionImpl implements NOMDetection {

    @Override
    public Long nomDetection(@NonNull ClassModel classModel) {
        if(classModel.getMethodModels() == null){
            return 0L;
        }
        return (long) classModel.getMethodModels().size();
    }
}