package com.tool.calculation.metrics.service.implementation;

import com.tool.calculation.metrics.model.ClassModel;
import com.tool.calculation.metrics.model.MethodModel;
import com.tool.calculation.metrics.service.CCDetection1;
import com.tool.calculation.metrics.service.WMCDetection;
import lombok.NonNull;
import org.springframework.stereotype.Service;

import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class CCDetectionImpl1 implements CCDetection1 {

    private final String forRegex = "^[^A-Za-z0-9]*for\\s*\\(.*\\)\\s*\\{";
    private final String whileRegex = "^[^A-Za-z0-9]*while\\s*\\(.*\\)\\s*\\{";
    private final String doRegex = "^[^A-Za-z0-9]*do\\s*\\{";
    private final String ifRegex = "^[^A-Za-z0-9]*if\\s*\\(.*\\)\\s*\\{";
    private final String elseifRegex = "^[^A-Za-z0-9]*else\\s*if\\s*\\(.*\\)\\s*\\{";
    private final String elseRegex = "^[^A-Za-z0-9]*else\\s*\\{";
    private final String tryRegex = "^[^A-Za-z0-9]*try\\s*\\{";
    private final String catchRegex = "^[^A-Za-z0-9]*catch\\s*\\(.*\\)\\s*\\{";

    Pattern forPattern = Pattern.compile(forRegex, Pattern.MULTILINE);
    Pattern whilePattern = Pattern.compile(whileRegex, Pattern.MULTILINE);
    Pattern doPattern = Pattern.compile(doRegex, Pattern.MULTILINE);
    Pattern ifPattern = Pattern.compile(ifRegex, Pattern.MULTILINE);
    Pattern elseifPattern = Pattern.compile(elseifRegex, Pattern.MULTILINE);
    Pattern elsePattern = Pattern.compile(elseRegex, Pattern.MULTILINE);
    Pattern tryPattern = Pattern.compile(tryRegex, Pattern.MULTILINE);
    Pattern catchPattern = Pattern.compile(catchRegex, Pattern.MULTILINE);

    @Override
    public Long CCDetection(@NonNull ClassModel classModel) {
        Long totalCC = 0L;

        if (classModel.getMethodModels() != null) {

            for (int i = 0; i < classModel.getMethodModels().size(); i++) {

                MethodModel methodModel = classModel.getMethodModels().get(i);
                String methodBody = methodModel.getBody();

                Long CC;

                long[] result = countWMC(methodBody);
                Long n = result[0];
                Long e = result[1];

//                System.out.println("CC n: " + n + ", e: " + e);

                CC = e - n + 2;
//                System.out.println("CC : " + CC);
                if(CC > 10)
                    totalCC += 1;

            }

        }

        return totalCC;
    }

    public long[] countWMC(String body) {

        Long n = 0L;
        Long e = 0L;
        String methodBody = body;

        Matcher forMatcher = forPattern.matcher(methodBody);
        Matcher whileMatcher = whilePattern.matcher(methodBody);
        Matcher doMatcher = doPattern.matcher(methodBody);
        Matcher ifMatcher = ifPattern.matcher(methodBody);
        Matcher tryMatcher = tryPattern.matcher(methodBody);

        String decisionType = "";
        int decisionIndexStart = -1;
        int decisionIndexEnd = -1;

        if (forMatcher.find()) {
            if(decisionIndexStart == -1 || forMatcher.start() < decisionIndexStart) {
                decisionType = "for";
                decisionIndexStart = forMatcher.start();
                decisionIndexEnd = forMatcher.end() - 1;
            }
        }
        if (whileMatcher.find()) {
            if(decisionIndexStart == -1 || whileMatcher.start() < decisionIndexStart) {
                decisionType = "while";
                decisionIndexStart = whileMatcher.start();
                decisionIndexEnd = whileMatcher.end() - 1;
            }
        }
        if (doMatcher.find()) {
            if(decisionIndexStart == -1 || doMatcher.start() < decisionIndexStart) {
                decisionType = "do";
                decisionIndexStart = doMatcher.start();
                decisionIndexEnd = doMatcher.end() - 1;
            }
        }
        if (ifMatcher.find()) {
            if(decisionIndexStart == -1 || ifMatcher.start() < decisionIndexStart) {
                decisionType = "if";
                decisionIndexStart = ifMatcher.start();
                decisionIndexEnd = ifMatcher.end() - 1;
            }
        }
        if (tryMatcher.find()) {
            if(decisionIndexStart == -1 || tryMatcher.start() < decisionIndexStart) {
                decisionType = "try";
                decisionIndexStart = tryMatcher.start();
                decisionIndexEnd = tryMatcher.end() - 1;
            }
        }


        if(decisionType.equals("for")) {
            String beforeFor = methodBody.substring(0, decisionIndexStart);

            //if statement found before 'for'
            if (!beforeFor.trim().equals("")) {
                n += 1;
                e += 1;
            }

            //add n and e => 'for' expression
            n += 1;
            e += 2;

            //get end of expression body
            int endofForIndex = -1;

            if (methodBody.charAt(decisionIndexEnd) == '{') {

                //find close curly bracket
                Stack<Integer> st = new Stack<>();
                for (int i = decisionIndexEnd; i < methodBody.length(); i++) {
                    if (methodBody.charAt(i) == '{') {
                        st.push((int) methodBody.charAt(i));
                    } else if (methodBody.charAt(i) == '}') {
                        st.pop();
                        if (st.empty()) {
                            endofForIndex = i;
                            break;
                        }
                    }
                }
            }

            String forBody = methodBody.substring(decisionIndexEnd + 1, endofForIndex);

            long[] resultForBody = countWMC(forBody);

            //sum the result from expressionBody
            n += resultForBody[0];
            e += resultForBody[1];

            //get the rest of code

            methodBody = methodBody.substring(endofForIndex + 1);

            long[] resultBelowFor = countWMC(methodBody);

            n += resultBelowFor[0];
            e += resultBelowFor[1];

            methodBody = "";
        }
        else if(decisionType.equals("while")) {
            String beforeWhile = methodBody.substring(0, decisionIndexStart);

            //if statement found before 'for|while'
            if (!beforeWhile.trim().equals("")) {
                n += 1;
                e += 1;
            }

            //add n and e => 'for|while expression'
            n += 1;
            e += 2;

            //get end of expression body
            int endofWhileIndex = -1;

            if (methodBody.charAt(decisionIndexEnd) == '{') {

                //find close curly bracket
                Stack<Integer> st = new Stack<>();
                for (int i = decisionIndexEnd; i < methodBody.length(); i++) {
                    if (methodBody.charAt(i) == '{') {
                        st.push((int) methodBody.charAt(i));
                    } else if (methodBody.charAt(i) == '}') {
                        st.pop();
                        if (st.empty()) {
                            endofWhileIndex = i;
                            break;
                        }
                    }
                }
            }

            String whileBody = methodBody.substring(decisionIndexEnd + 1, endofWhileIndex);

            long[] resultWhileBody = countWMC(whileBody);

            //sum the result from expressionBody
            n += resultWhileBody[0];
            e += resultWhileBody[1];

            //get the rest of code

            methodBody = methodBody.substring(endofWhileIndex + 1);

            long[] resultBelowWhile = countWMC(methodBody);

            n += resultBelowWhile[0];
            e += resultBelowWhile[1];

            methodBody = "";
        }
        else if(decisionType.equals("do")) {
            String beforeDo = methodBody.substring(0, decisionIndexStart);

            //if statement found before 'do'
            if (!beforeDo.trim().equals("")) {
                n += 1;
                e += 1;
            }

            //get end of expression body
            int endofDoIndex = -1;

            if (methodBody.charAt(decisionIndexEnd) == '{') {

                //find close curly bracket
                Stack<Integer> st = new Stack<>();
                for (int i = decisionIndexEnd; i < methodBody.length(); i++) {
                    if (methodBody.charAt(i) == '{') {
                        st.push((int) methodBody.charAt(i));
                    } else if (methodBody.charAt(i) == '}') {
                        st.pop();
                        if (st.empty()) {
                            endofDoIndex = i;
                            break;
                        }
                    }
                }
            }

            String doBody = methodBody.substring(decisionIndexEnd + 1, endofDoIndex);

            long[] resultDoBody = countWMC(doBody);

            //sum the result from expressionBody
            n += resultDoBody[0];
            e += resultDoBody[1];

            //add n and e => 'while'
            n += 1;
            e += 2;

            int endofDoWhileIndex = -1;

            for (int i = (endofDoIndex + 1); i < methodBody.length(); i++) {
                if (methodBody.charAt(i) == ';') {
                    endofDoWhileIndex = i;
                    break;
                }
            }

            //get n, e from code below for

            methodBody = methodBody.substring(endofDoWhileIndex + 1);

            long[] resultBelowDo = countWMC(methodBody);

            n += resultBelowDo[0];
            e += resultBelowDo[1];

            methodBody = "";
        }
        else if(decisionType.equals("if")) {
            String beforeIf = methodBody.substring(0, decisionIndexStart);

            //if statement found before 'if'
            if (!beforeIf.trim().equals("")) {
                n += 1;
                e += 1;
            }

            //add n and e => 'if' expression
            n += 1;
            e += 2;

            //get end of expression body
            int endofIfIndex = -1;

            if (methodBody.charAt(decisionIndexEnd) == '{') {

                //find close curly bracket
                Stack<Integer> st = new Stack<>();
                for (int i = decisionIndexEnd; i < methodBody.length(); i++) {
                    if (methodBody.charAt(i) == '{') {
                        st.push((int) methodBody.charAt(i));
                    } else if (methodBody.charAt(i) == '}') {
                        st.pop();
                        if (st.empty()) {
                            endofIfIndex = i;
                            break;
                        }
                    }
                }
            }

            String ifBody = methodBody.substring(decisionIndexEnd + 1, endofIfIndex);

            long[] resultIfBody = countWMC(ifBody);

            //sum the result from expressionBody
            n += resultIfBody[0];
            e += resultIfBody[1];

            //get the rest of code

            methodBody = methodBody.substring(endofIfIndex + 1);

            //check if followed by else if
            while(true) {
                Matcher elseifMatcher = elseifPattern.matcher(methodBody);

                if (elseifMatcher.find()) {
                    if(methodBody.substring(0, elseifMatcher.start()).trim().equals("")) {
                        //add n and e => 'else if' expression
                        n += 1;
                        e += 2;

                        int endofElseifIndex = -1;
                        int elseifOpenBracketIndex = elseifMatcher.end() - 1;

                        if (methodBody.charAt(elseifOpenBracketIndex) == '{') {

                            //find close curly bracket
                            Stack<Integer> st = new Stack<>();
                            for (int i = elseifOpenBracketIndex; i < methodBody.length(); i++) {
                                if (methodBody.charAt(i) == '{') {
                                    st.push((int) methodBody.charAt(i));
                                } else if (methodBody.charAt(i) == '}') {
                                    st.pop();
                                    if (st.empty()) {
                                        endofElseifIndex = i;
                                        break;
                                    }
                                }
                            }
                        }

                        String elseifBody = methodBody.substring(elseifOpenBracketIndex + 1, endofElseifIndex);

                        long[] resultElseifBody = countWMC(elseifBody);

                        //sum the result from expressionBody
                        n += resultElseifBody[0];
                        e += resultElseifBody[1];

                        //get the rest of code
                        methodBody = methodBody.substring(endofElseifIndex);
                    }
                    else {
                        break;
                    }
                }
                else {
                    break;
                }
            }

            Matcher elseMatcher = elsePattern.matcher(methodBody);

            if (elseMatcher.find()) {
                if(methodBody.substring(0, elseMatcher.start()).trim().equals("")) {
                    //add n and e => 'else if' expression
                    n += 1;
                    e += 1;

                    int endofElseIndex = -1;
                    int elseOpenBracketIndex = elseMatcher.end() - 1;

                    if (methodBody.charAt(elseOpenBracketIndex) == '{') {

                        //find close curly bracket
                        Stack<Integer> st = new Stack<>();
                        for (int i = elseOpenBracketIndex; i < methodBody.length(); i++) {
                            if (methodBody.charAt(i) == '{') {
                                st.push((int) methodBody.charAt(i));
                            } else if (methodBody.charAt(i) == '}') {
                                st.pop();
                                if (st.empty()) {
                                    endofElseIndex = i;
                                    break;
                                }
                            }
                        }
                    }

                    String elseBody = methodBody.substring(elseOpenBracketIndex + 1, endofElseIndex);

                    long[] resultElseBody = countWMC(elseBody);

                    //sum the result from expressionBody
                    n += resultElseBody[0];
                    e += resultElseBody[1];

                    //get the rest of code
                    methodBody = methodBody.substring(endofElseIndex);
                }
            }

            long[] resultBelowIfElse = countWMC(methodBody);

            n += resultBelowIfElse[0];
            e += resultBelowIfElse[1];

            methodBody = "";
        }
        else if(decisionType.equals("try")) {
            String beforeTry = methodBody.substring(0, decisionIndexStart);

            //if statement found before 'try'
            if (!beforeTry.trim().equals("")) {
                n += 1;
                e += 1;
            }

            //get end of 'try' body
            int endofTryIndex = -1;

            if (methodBody.charAt(decisionIndexEnd) == '{') {

                //find close curly bracket
                Stack<Integer> st = new Stack<>();
                for (int i = decisionIndexEnd; i < methodBody.length(); i++) {
                    if (methodBody.charAt(i) == '{') {
                        st.push((int) methodBody.charAt(i));
                    } else if (methodBody.charAt(i) == '}') {
                        st.pop();
                        if (st.empty()) {
                            endofTryIndex = i;
                            break;
                        }
                    }
                }
            }

            String tryBody = methodBody.substring(decisionIndexEnd + 1, endofTryIndex);

            long[] resultTryBody = countWMC(tryBody);

            //sum the result from expressionBody
            n += resultTryBody[0];
            e += resultTryBody[1];

            //get the rest of code

            methodBody = methodBody.substring(endofTryIndex + 1);

            //check if followed by else if
            while(true) {
                Matcher catchMatcher = catchPattern.matcher(methodBody);

                if (catchMatcher.find()) {
                    if(methodBody.substring(0, catchMatcher.start()).trim().equals("")) {
                        //add n and e => 'catch' expression
                        n += 1;
                        e += 2;

                        int endofCatchIndex = -1;
                        int catchOpenBracketIndex = catchMatcher.end() - 1;

                        if (methodBody.charAt(catchOpenBracketIndex) == '{') {

                            //find close curly bracket
                            Stack<Integer> st = new Stack<>();
                            for (int i = catchOpenBracketIndex; i < methodBody.length(); i++) {
                                if (methodBody.charAt(i) == '{') {
                                    st.push((int) methodBody.charAt(i));
                                } else if (methodBody.charAt(i) == '}') {
                                    st.pop();
                                    if (st.empty()) {
                                        endofCatchIndex = i;
                                        break;
                                    }
                                }
                            }
                        }

                        String catchBody = methodBody.substring(catchOpenBracketIndex + 1, endofCatchIndex);

                        long[] resultCatchBody = countWMC(catchBody);

                        //sum the result from expressionBody
                        n += resultCatchBody[0];
                        e += resultCatchBody[1];

                        //get the rest of code
                        methodBody = methodBody.substring(endofCatchIndex);
                    }
                    else {
                        break;
                    }
                }
                else {
                    break;
                }
            }

            long[] resultBelowTryCatch = countWMC(methodBody);

            n += resultBelowTryCatch[0];
            e += resultBelowTryCatch[1];

            methodBody = "";
        }



        if (!methodBody.trim().equals("")) {
            n += 1;
            e += 1;
        }

        return new long[] {n, e};

    }
}

