package com.tool.calculation.metrics.service;

import com.tool.calculation.metrics.model.FileModel;
import lombok.NonNull;

public interface ClassesDetectionUtil {
    String getClassKey(@NonNull FileModel fileModel);
}
