package com.tool.calculation.metrics.service.implementation;

import com.tool.calculation.metrics.model.ClassModel;
import com.tool.calculation.metrics.service.NOCDetection;
import lombok.NonNull;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class NOCDetectionImpl implements NOCDetection {

    @Override
    public Long NOCDetection(@NonNull ClassModel classModel, @NonNull Map<String, List<ClassModel>> classes) {
        Long totalNOC = 0L;

        for (Map.Entry<String, List<ClassModel>> entry : classes.entrySet()) {
            for(ClassModel model : entry.getValue()) {
                //System.out.println(model.getExtend() + " " + classModel.getName());
                if(model.getExtend() != null) {
                    if(model.getExtend().equals(classModel.getName())) {
                    totalNOC++;
                    }
                }
            }
        }

        return totalNOC;
    }

}
