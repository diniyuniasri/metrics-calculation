package com.tool.calculation.metrics.service;

import com.tool.calculation.metrics.model.ClassModel;
import lombok.NonNull;

public interface NOFDetection {
    Long nofDetection(@NonNull ClassModel classModel);
}