package com.tool.calculation.metrics.service;

import com.tool.calculation.metrics.model.ClassIndexModel;
import com.tool.calculation.metrics.model.FileModel;
import com.tool.calculation.metrics.model.MethodModel;
import lombok.NonNull;

import java.util.List;
import java.util.Map;

/**
 * @author M. Dini Yuniasri
 * @version 1.0.0
 * @since 27 April 2019
 */

public interface MethodsDetectionThread {

    void detect(@NonNull FileModel fileModel, @NonNull Map<String, ClassIndexModel> classIndexModels, @NonNull Map<String, List<MethodModel>> result);
}
