package com.tool.calculation.metrics.service;

import com.tool.calculation.metrics.model.ClassModel;
import com.tool.calculation.metrics.model.FileModel;
import lombok.NonNull;

import java.util.List;
import java.util.Map;

public interface ClassesDetection {
    List<ClassModel> detect(@NonNull FileModel fileModel);

    Map<String, List<ClassModel>> detect(@NonNull List<FileModel> fileModels);
}
