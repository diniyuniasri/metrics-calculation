package com.tool.calculation.metrics.service.implementation;

import com.tool.calculation.metrics.model.ClassIndexModel;
import com.tool.calculation.metrics.model.FileModel;
import com.tool.calculation.metrics.model.MethodModel;
import com.tool.calculation.metrics.service.MethodsDetection;
import com.tool.calculation.metrics.service.MethodsDetectionThread;
import com.tool.calculation.metrics.service.MethodsDetectionUtil;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @author M. Dini Yuniasri
 * @version 1.0.0
 * @since 27 April 2019
 */

@Service
public class MethodsDetectionImpl implements MethodsDetection {

    @Autowired
    private MethodsDetectionThread methodsDetectionThread;

    @Autowired
    private MethodsDetectionUtil methodsDetectionUtil;

    @Override
    public List<MethodModel> detect(@NonNull FileModel fileModel, @NonNull ClassIndexModel classIndexModel) {
        return detect(Collections.singletonList(fileModel), Collections.singletonMap(fileModel.getFullPath(), classIndexModel))
                .get(methodsDetectionUtil.getMethodKey(fileModel));
    }

    @Override
    public Map<String, List<MethodModel>> detect(@NonNull List<FileModel> fileModels, @NonNull Map<String, ClassIndexModel> classIndexModels) {
        Map<String, List<MethodModel>> result = new HashMap<>();
        doMethodsDetection(fileModels, classIndexModels, result);

        return result;
    }

    private void doMethodsDetection(List<FileModel> fileModels, Map<String, ClassIndexModel> classIndexModels, Map<String, List<MethodModel>> result) {
        fileModels.parallelStream()
                .forEach(fileModel -> detectMethods(fileModel, classIndexModels, result));
    }

    private void detectMethods(FileModel fileModel, Map<String, ClassIndexModel> classIndexModels, Map<String, List<MethodModel>> result) {
        String key = methodsDetectionUtil.getMethodKey(fileModel);
        result.put(key, Collections.synchronizedList(new ArrayList<>()));

        methodsDetectionThread.detect(fileModel, classIndexModels, result);
    }
}
