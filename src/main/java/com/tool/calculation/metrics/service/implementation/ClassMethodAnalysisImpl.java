package com.tool.calculation.metrics.service.implementation;

import com.tool.calculation.metrics.model.ClassIndexModel;
import com.tool.calculation.metrics.model.ClassModel;
import com.tool.calculation.metrics.model.FileModel;
import com.tool.calculation.metrics.model.MethodModel;
import com.tool.calculation.metrics.service.ClassMethodAnalysis;
import com.tool.calculation.metrics.service.MethodsDetection;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClassMethodAnalysisImpl implements ClassMethodAnalysis {

    @Autowired
    private MethodsDetection methodsDetection;

    @Override
    public void analysis(@NonNull FileModel fileModel, @NonNull ClassModel classModel, @NonNull ClassIndexModel classIndexModel) {
        List<MethodModel> methods = methodsDetection.detect(fileModel, classIndexModel);
        classModel.setMethodModels(methods);
    }
}
