package com.tool.calculation.metrics.service.implementation;

import com.tool.calculation.metrics.model.AttributeModel;
import com.tool.calculation.metrics.model.ClassModel;
import com.tool.calculation.metrics.model.MethodModel;
import com.tool.calculation.metrics.service.LCOMDetection;
import lombok.NonNull;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class LCOMDetectionImpl implements LCOMDetection{

    @Override
    public Long LCOMDetection(@NonNull ClassModel classModel) {
        Long P = 0L;
        Long Q = 0L;

        if(classModel.getMethodModels() != null) {
//            System.out.println("CLASS:" + classModel.getName());
            List<String> classInstanceNames = new ArrayList<>();

            List<AttributeModel> classInstances = classModel.getAttributes();
            for(AttributeModel attribute: classInstances) {
//                System.out.println("instance: " + attribute.getType() +"|||"+ attribute.getName() + "|||" + attribute.getValue());
                classInstanceNames.add(attribute.getName());
            }

            for (int i = 0; i < classModel.getMethodModels().size(); i++) {

                MethodModel methodModel1 = classModel.getMethodModels().get(i);

                for (int j = i + 1; j < classModel.getMethodModels().size(); j++) {

                    MethodModel methodModel2 = classModel.getMethodModels().get(j);
//                            System.out.println("check " + methodModel1.getName() + " with " + methodModel2.getName());

                    if(isUsingSameInstance(classInstanceNames, methodModel1.getBody(), methodModel2.getBody())) {
                        P++;
//                                System.out.println("true found");
                    }
                    else {
//                                System.out.println("false found");
                        Q++;
                    }

                }
            }

        }

        return defineLCOM(P, Q);
    }

    public Long defineLCOM(Long P, Long Q) {
//        System.out.println("P:" + P + " Q:" + Q);
        if(P > Q)
            return (P - Q);
        else
            return 0L;
    }

    public Boolean isUsingSameInstance(List<String> instances, String methodBody, String methodBody2) {
        Boolean isUsingSame = false;

        for(String instance: instances) {
            Boolean usedByMethod1 = false;
            Boolean usedByMethod2 = false;

            Pattern methodPattern = Pattern.compile(instance, Pattern.MULTILINE);

            Matcher methodMatcher = methodPattern.matcher(methodBody);
            while (methodMatcher.find()) {
                char before = methodBody.charAt(methodMatcher.start() - 1);
                char after = methodBody.charAt(methodMatcher.end());

                if (!((before >= 'A' && before <= 'Z') || (before >= 'a' && before <= 'z') || (after >= 'A' && after <= 'Z') || (after >= 'a' && after <= 'z'))) {
//                    System.out.println("verified1:" + methodBody.substring(methodMatcher.start(), methodMatcher.end()));
                    usedByMethod1 = true;
                    break;
                }
            }

            Matcher methodMatcher2 = methodPattern.matcher(methodBody2);
            while (methodMatcher2.find()) {
                char before = methodBody2.charAt(methodMatcher2.start() - 1);
                char after = methodBody2.charAt(methodMatcher2.end());

                if (!((before >= 'A' && before <= 'Z') || (before >= 'a' && before <= 'z') || (after >= 'A' && after <= 'Z') || (after >= 'a' && after <= 'z'))) {
//                    System.out.println("verified2:" + methodBody2.substring(methodMatcher2.start(), methodMatcher2.end()));
                    usedByMethod2 = true;
                    break;
                }
            }

            if(usedByMethod1 && usedByMethod2) {
                isUsingSame = true;
                break;
            }
        }

        return isUsingSame;
    }
}

