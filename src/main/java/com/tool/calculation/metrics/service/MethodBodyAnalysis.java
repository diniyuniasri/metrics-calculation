package com.tool.calculation.metrics.service;

import com.tool.calculation.metrics.model.IndexModel;
import com.tool.calculation.metrics.model.MethodModel;
import lombok.NonNull;

/**
 * @author M. Dini Yuniasri
 * @version 1.0.0
 * @since 27 April 2019
 */

public interface MethodBodyAnalysis {

    void analysis(@NonNull String content, @NonNull IndexModel indexModel,
                  @NonNull MethodModel methodModel);
}
