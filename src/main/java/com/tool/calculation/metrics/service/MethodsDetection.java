package com.tool.calculation.metrics.service;

import com.tool.calculation.metrics.model.ClassIndexModel;
import com.tool.calculation.metrics.model.FileModel;
import com.tool.calculation.metrics.model.MethodModel;
import lombok.NonNull;

import java.util.List;
import java.util.Map;

/**
 * @author M. Dini Yuniasri
 * @version 1.0.0
 * @since 27 April 2019
 */

public interface MethodsDetection {

    List<MethodModel> detect(@NonNull FileModel fileModel, @NonNull ClassIndexModel classIndexModel);

    Map<String, List<MethodModel>> detect(@NonNull List<FileModel> fileModels, @NonNull Map<String, ClassIndexModel> classIndexModels);
}
