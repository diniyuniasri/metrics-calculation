package com.tool.calculation.metrics.service.implementation;

import com.tool.calculation.metrics.model.ClassModel;
import com.tool.calculation.metrics.model.FileModel;
import com.tool.calculation.metrics.model.ClassIndexModel;
import com.tool.calculation.metrics.service.ClassAnalysis;
import com.tool.calculation.metrics.service.ClassesDetectionThread;
import com.tool.calculation.metrics.service.ThreadsWatcher;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.concurrent.Future;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
public class ClassesDetectionThreadImpl implements ClassesDetectionThread {

    @Autowired
    private ClassAnalysis classAnalysis;

    @Autowired
    private ThreadsWatcher threadsWatcher;

    @Value("${threads.waiting.time}")
    private Integer waitingTime;

    private String classesRegex = ".*class\\s+(\\w+)(\\s+extends\\s+(\\w+))?(\\s+implements\\s+([\\w,\\s]+))?([\\w,\\s,.,<,>]+)?\\s*\\{.*$";

    @Async
    @Override
    public Future detect(@NonNull FileModel fileModel, @NonNull Map<String, List<ClassModel>> result) {
//        System.out.println("file: " + fileModel.getFilename());
        List<ClassIndexModel> indexOfClasses = getIndexOfClasses(fileModel.getContent());
        List<Future> threads = doAnalysisClasses(indexOfClasses, fileModel, result);

        threadsWatcher.waitAllThreadsDone(threads, waitingTime);

        return null;
    }

    private List<ClassIndexModel> getIndexOfClasses(String content) {
        Pattern pattern = Pattern.compile(classesRegex, Pattern.MULTILINE);
        Matcher matcher = pattern.matcher(content);

        return findIndex(matcher, content);
    }

    private List<ClassIndexModel> findIndex(Matcher matcher, String content) {
        List<ClassIndexModel> indexModels = new ArrayList<>();

        while (matcher.find()) {
            saveIndex(matcher, indexModels, content);
        }

        return indexModels;
    }

    private void saveIndex(Matcher matcher, List<ClassIndexModel> indexModels, String content) {
//        System.out.println("proceed: " + matcher.start() + " " + matcher.end());
        int endDeclarationIndex = 0;
        for (int i = matcher.end(); i > matcher.start(); i--) {
            if (content.charAt(i) == '{') {
                endDeclarationIndex = i;
                break;
            }
        }
        int endClassIndex = 0;

//        System.out.println("content: " + content.substring(matcher.start(), endDeclarationIndex));


        if (content.charAt(endDeclarationIndex) == '{') {
//            System.out.println("proceed search: " + matcher.start() + " " + matcher.end());

            Stack<Integer> st = new Stack<>();
            for (int i = endDeclarationIndex; i < content.length(); i++) {
                if (content.charAt(i) == '{') {
                    st.push((int) content.charAt(i));
                }
                else if (content.charAt(i) == '}') {
                    st.pop();
                    if (st.empty()) {
//                        System.out.println("found final" + i);
                        endClassIndex = i;
                        break;
                    }
                }
            }
        }

        ClassIndexModel classIndexModel = ClassIndexModel.builder()
                .startDeclaration(matcher.start())
                .endDeclaration(endDeclarationIndex)
                .endClass(endClassIndex)
                .build();

        indexModels.add(classIndexModel);
    }

    private List<Future> doAnalysisClasses(List<ClassIndexModel> indexOfClasses, FileModel fileModel, Map<String, List<ClassModel>> result) {
        return indexOfClasses.stream()
                .map(indexOfClass -> classAnalysis.analysis(fileModel, indexOfClass, result))
                .collect(Collectors.toList());
    }
}
