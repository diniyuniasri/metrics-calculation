package com.tool.calculation.metrics.service.implementation;


import com.tool.calculation.metrics.model.FileModel;
import com.tool.calculation.metrics.model.IndexModel;
import com.tool.calculation.metrics.model.MethodModel;
import com.tool.calculation.metrics.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @author M. Dini Yuniasri
 * @version 1.0.0
 * @since 27 April 2019
 */

@Service
public class JavaMethodAnalysis implements MethodAnalysis {

    @Autowired
    private StartIndexAnalysis startIndexAnalysis;

    @Autowired
    private MethodAttributesAnalysis methodAttributesAnalysis;

    @Autowired
    private MethodBodyAnalysis methodBodyAnalysis;

    @Autowired
    private MethodStatementAnalysis methodStatementAnalysis;

    @Autowired
    private MethodsDetectionUtil methodsDetectionUtil;

    @Override
    public void analysis(FileModel fileModel, String classBody, IndexModel indexModel, Map<String, List<MethodModel>> result) {
        MethodModel methodModel = MethodModel.builder().build();

        try {
            startIndexAnalysis.analysis(classBody, indexModel);
            methodAttributesAnalysis.analysis(fileModel, classBody, indexModel, methodModel);
            methodBodyAnalysis.analysis(classBody, indexModel, methodModel);
            methodStatementAnalysis.analysis(methodModel);
            saveResult(fileModel, methodModel, result);
        } catch (Exception e) {
            // Do nothing
            // Mark of non-method analysis
        }
//        System.out.println("Method: " + methodModel.getName());
//        System.out.println(Arrays.toString(methodModel.getKeywords().toArray()));
//        System.out.println("--------------------------------------------------------------");
    }

    private void saveResult(FileModel fileModel, MethodModel methodModel,
                            Map<String, List<MethodModel>> result) {
        String key = methodsDetectionUtil.getMethodKey(fileModel);
        result.get(key)
                .add(methodModel);
    }
}
