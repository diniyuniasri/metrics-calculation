package com.tool.calculation.metrics.service.implementation;

import com.tool.calculation.metrics.model.AttributeModel;
import com.tool.calculation.metrics.model.ClassModel;
import com.tool.calculation.metrics.model.MethodModel;
import com.tool.calculation.metrics.service.CBODetection;
import com.tool.calculation.metrics.service.CCCDetection;
import lombok.NonNull;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class CCCDetectionImpl implements CCCDetection {

    public Long CCCDetection(@NonNull ClassModel classModel, Map<String, List<ClassModel>> classes) {
        Long totalCCC = 0L;

        String className = classModel.getName();
        String classDeclarationRegex = "new\\s+"+className+"\\s*\\(.*\\)";
        Pattern classDeclarationPattern = Pattern.compile(classDeclarationRegex, Pattern.MULTILINE);

//        System.out.println("ClassName: " + className);

        for (Map.Entry<String, List<ClassModel>> entry : classes.entrySet()) {
            for(ClassModel model : entry.getValue()) {

                if(!model.getName().equals(className)) {
                    //check CBO in other class

                    //check any class declaration in other class instances
                    Boolean foundInInstance = false;

                    if (model.getAttributes() != null) {

                        for (int i = 0; i < model.getAttributes().size(); i++) {

                            AttributeModel attributeModel = model.getAttributes().get(i);
                            String attributeValue = attributeModel.getValue();

                            Matcher classDeclarationMatcher = classDeclarationPattern.matcher(attributeValue);

                            if (classDeclarationMatcher.find()) {

                                String attributeName = attributeModel.getName();

                                //search method or variable usage in class body

                                String attributeUsageRegex = "[^A-Za-z0-9]"+attributeName+"\\.";
                                Pattern attributeUsagePattern = Pattern.compile(attributeUsageRegex, Pattern.MULTILINE);
                                Matcher attributeUsageMatcher = attributeUsagePattern.matcher(model.getFullContent());

                                if (attributeUsageMatcher.find()) {
                                    totalCCC++;
                                    break;
                                }

                            }

                        }
                    }

                    if(totalCCC > 0) {
                        break;
                    }

                    //check any class declaration in other class methods
                    if (model.getMethodModels() != null) {

                        for (int i = 0; i < model.getMethodModels().size(); i++) {

                            MethodModel methodModel = model.getMethodModels().get(i);
                            String methodBody = methodModel.getBody();

                            Matcher classDeclarationMatcher = classDeclarationPattern.matcher(methodBody);

                            if (classDeclarationMatcher.find()) {

                                int attrIndexStart = 0;
                                int attrIndexEnd = 0;
                                Boolean foundEqual = false;
                                Boolean foundAttribute = false;

                                for (int j = (classDeclarationMatcher.start() - 1); j >= 0; j--) {
                                    if (!foundEqual && methodBody.charAt(j) == '=') {
                                        foundEqual = true;
                                        continue;
                                    }
                                    if(foundEqual && !foundAttribute && methodBody.charAt(j) != ' ' && methodBody.charAt(j) != '\n' && methodBody.charAt(j) != '\r') {
                                        attrIndexEnd = j;
                                        foundAttribute = true;
                                        continue;
                                    }
                                    if(foundAttribute && (methodBody.charAt(j) == ' ' || methodBody.charAt(j) == '\n' || methodBody.charAt(j) == '\r' || methodBody.charAt(j) == ';' || methodBody.charAt(j) == '{' || methodBody.charAt(j) == '(')){
                                        attrIndexStart = j+1;
                                        break;
                                    }
                                }

                                String attributeName = "";

                                if(attrIndexEnd > attrIndexStart){
                                    attributeName = methodBody.substring(attrIndexStart, attrIndexEnd+1).trim();
                                }

                                //search method or variable usage in method body below declaration found

                                if(attributeName.length() > 0){
//                                    System.out.println("attributeName : " + attributeName);
                                    String attributeUsageRegex = "[^A-Za-z0-9]"+attributeName+"\\.";
                                    Pattern attributeUsagePattern = Pattern.compile(attributeUsageRegex, Pattern.MULTILINE);
                                    Matcher attributeUsageMatcher = attributeUsagePattern.matcher(methodBody.substring(classDeclarationMatcher.end()));

                                    if (attributeUsageMatcher.find()) {
                                        totalCCC++;
                                        break;
                                    }
                                }

                            }

                        }

                    }

                    if(totalCCC > 0) {
                        break;
                    }


                }

            }

            if(totalCCC > 0) {
                break;
            }

        }

        return totalCCC;
    }
}
