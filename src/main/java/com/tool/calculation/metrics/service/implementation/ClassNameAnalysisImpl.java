package com.tool.calculation.metrics.service.implementation;

import com.tool.calculation.metrics.model.ClassIndexModel;
import com.tool.calculation.metrics.model.ClassModel;
import com.tool.calculation.metrics.model.FileModel;
import com.tool.calculation.metrics.model.IndexModel;
import com.tool.calculation.metrics.service.ClassNameAnalysis;
import lombok.NonNull;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class ClassNameAnalysisImpl implements ClassNameAnalysis {


    @Override
    public void analysis(@NonNull FileModel fileModel, @NonNull ClassModel classModel, @NonNull ClassIndexModel indexModel) {
        String className = fileModel.getContent().substring(indexModel.getStartDeclaration(), indexModel.getEndDeclaration()).trim();
        String[] list = className.split(" ");
        for(int i = 0; i < list.length; i++){
            if(list[i].equals("class")){
                classModel.setName(list[i+1].replace("{",""));
            }
            else if(list[i].equals("extends")){
                classModel.setExtend(list[i+1].replace("{",""));
            }
            else if(list[i].equals("implements")){
                classModel.setImplement(list[i+1].replace("{",""));
            }
        }
        Integer indexclass = className.indexOf("class");
        List<String> keywords = Arrays.asList(className.substring(0, indexclass - 1).trim().split(" "));
        classModel.setKeywords(keywords);
    }
}
