package com.tool.calculation.metrics.service.implementation;

import com.tool.calculation.metrics.model.ClassIndexModel;
import com.tool.calculation.metrics.model.FileModel;
import com.tool.calculation.metrics.model.IndexModel;
import com.tool.calculation.metrics.model.MethodModel;
import com.tool.calculation.metrics.service.MethodAnalysis;
import com.tool.calculation.metrics.service.MethodsDetectionThread;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author M. Dini Yuniasri
 * @version 1.0.0
 * @since 27 April 2019
 */

@Service
public class MethodsDetectionThreadImpl implements MethodsDetectionThread {

    @Autowired
    private MethodAnalysis methodAnalysis;

    @Value("${methods.detection.regex}")
    private String methodsRegex;

    @Override
    public void detect(@NonNull FileModel fileModel, @NonNull Map<String, ClassIndexModel> classIndexModels, @NonNull Map<String, List<MethodModel>> result) {
        ClassIndexModel classIndexModel = classIndexModels.get(fileModel.getFullPath());
        String classBody = fileModel.getContent().substring(classIndexModel.getEndDeclaration(), classIndexModel.getEndClass() + 1);
        List<IndexModel> indexOfMethods = getIndexOfMethods(classBody);
        doAnalysisMethods(indexOfMethods, fileModel, classBody, result);
    }

    private List<IndexModel> getIndexOfMethods(String content) {
        Pattern pattern = Pattern.compile(methodsRegex, Pattern.MULTILINE);
        Matcher matcher = pattern.matcher(content);

        return findIndex(matcher);
    }

    private List<IndexModel> findIndex(Matcher matcher) {
        List<IndexModel> indexModels = new ArrayList<>();

        while (matcher.find()) {
            saveIndex(matcher, indexModels);
        }

        return indexModels;
    }

    private void saveIndex(Matcher matcher, List<IndexModel> indexModels) {
        IndexModel indexModel = IndexModel.builder()
                .start(matcher.start())
                .end(matcher.end())
                .build();

        indexModels.add(indexModel);
    }

    private void doAnalysisMethods(List<IndexModel> indexOfMethods, FileModel fileModel, String classBody,
                                   Map<String, List<MethodModel>> result) {
        indexOfMethods.parallelStream()
                .forEach(indexOfMethod -> methodAnalysis.analysis(fileModel, classBody, indexOfMethod, result));
    }
}
