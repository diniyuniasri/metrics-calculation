package com.tool.calculation.metrics.service;

import com.tool.calculation.metrics.model.FileModel;
import lombok.NonNull;

import java.util.List;
import java.util.Map;

public interface FilesDetection {

    List<FileModel> detect(@NonNull String path, @NonNull String mimeType);

    Map<String, List<FileModel>> detect(@NonNull List<String> paths, @NonNull String mimeType);
}
