package com.tool.calculation.metrics.service.implementation;

import com.tool.calculation.metrics.model.ClassIndexModel;
import com.tool.calculation.metrics.model.ClassModel;
import com.tool.calculation.metrics.model.FileModel;
import com.tool.calculation.metrics.model.MethodModel;
import com.tool.calculation.metrics.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class MainImpl implements Main {

    private String printable = "";

    @Autowired
    private FilesDetection filesDetection;

    @Autowired
    private ClassesDetection classesDetection;

    @Autowired
    private LocsDetection locsDetection;

    @Autowired
    private NOMDetection nomDetection;

    @Autowired
    private NOFDetection nofDetection;

    @Autowired
    private DITDetection ditDetection;

    @Autowired
    private NOCDetection nocDetection;

    @Autowired
    private LCOMDetection lcomDetection;

    @Autowired
    private RFCDetection rfcDetection;

    @Autowired
    private WMCDetection wmcDetection;

    @Autowired
    private CBODetection cboDetection;

    @Autowired
    private CCDetection1 ccDetection1;

    @Autowired
    private CCCDetection cccDetection;

    @Value("${files.mime.type}")
    private String mimeType;

    private List<ClassModel> allClasses = new ArrayList<ClassModel>();

    @Override
    public String doMetricsCalculation(List<String> paths) {
        printable = "";
        printable = printable + "<b>Detecting files...</b><br />";
        Map<String, List<FileModel>> files = filesDetection.detect(paths, mimeType);
        files.forEach(this::doReadFiles);
        return printable;
    }

    private void doReadFiles(String path, List<FileModel> fileModels) {
        Map<String, List<ClassModel>> classes = classesDetection.detect(fileModels);

        printable = printable + "Project Path -> " + path + "<br />";
        printable = printable + "Files detected -> " + fileModels.size() + "<br />";
//        fileModels.forEach(this::doPrintFile);

        int countClasses = 0;
        int countMethods = 0;

        for (Map.Entry<String, List<ClassModel>> entry : classes.entrySet()) {
            countClasses += entry.getValue().size();
        }

        printable = printable + "<br /><b>Detecting classes...</b><br />";
        printable = printable + "Class detected -> " + countClasses;
//        printable = printable + "Method detected -> " + countMethods;
//        classes.forEach(this::doPrintClass);

        printable = printable + printTable(path, fileModels, classes);
    }

    private void doPrintFile(FileModel fileModel) {
        printable = printable + "<br />Name : " + fileModel.getFilename() + "<br />";
        printable = printable + "Path : " + fileModel.getPath() + "<br />";
//        printable = printable + "Content : " + fileModel.getContent() + "\n   ";
    }

    private void searchClasses(String s, List<ClassModel> classModels) {
        classModels.forEach((temp) ->{
            allClasses.add(temp);
        });
    }

    private void doPrintClass(String s, List<ClassModel> classModels) {
        classModels.forEach((temp) ->{
            printable = printable + "<br />Name : " + temp.getName() + "<br />";
            printable = printable + "Path : " + temp.getPath() + "<br />";
        });

    }

    private String printTable(String path, List<FileModel> fileModels, Map<String, List<ClassModel>> classes) {
        String content = "";

        content = content + "<br /><br /><b>Table: " + path.split("\\\\")[path.split("\\\\").length - 1];

        content = content + "<table>";
        content = content + "<tr>";
        content = content + "<th>#</th>";
        content = content + "<th>Filename</th>";
        content = content + "<th>Classes</th>";
        /*content = content + "<th>LOC</th>";
        content = content + "<th>NOM</th>";
        content = content + "<th>NOF</th>";*/
        content = content + "<th>DIT</th>";
        content = content + "<th>NOC</th>";
        content = content + "<th>LCOM</th>";
        content = content + "<th>RFC</th>";
        content = content + "<th>WMC</th>";
        content = content + "<th>CBO</th>";
        content = content + "<th>CC > 10</th>";
        content = content + "<th>CCC</th>";
        content = content + "</tr>";

        int indexFile = 1;
        int countClasses = 0;
        Long totalLOC = 0L;
        Long totalNOM = 0L;
        Long totalNOF = 0L;
        Long maxDIT = 0L;
        Long totalNOC = 0L;
        Long totalLCOM =0L;
        Long totalRFC =0L;
        Long totalWMC =0L;
        Long totalCBO =0L;
        Long totalCC =0L;
        Long totalCCC =0L;
        DecimalFormat df = new DecimalFormat("#.###");

        for (FileModel model : fileModels) {
            content = content + "<tr>";
            content = content + "<td>" + indexFile +"</td>";
            content = content + "<td>" + model.getFilename() + "</td>";

            boolean found = false;

            for (Map.Entry<String, List<ClassModel>> entry : classes.entrySet()) {
                if(entry.getKey().equalsIgnoreCase(model.getFullPath())) {

//                    CLASS NAME
                    content = content + "<td>";

                    for (ClassModel m : entry.getValue()) {
                        content = content + m.getName() + " ";
                        //content = content + "\n" + m.getExtend() + " ";
                    }

                    content = content + "</td>";

//                    LOC
//                    content = content + "<td>";
//
//                    for (ClassModel m : entry.getValue()) {
//                        Long loc = locsDetection.locDetection(m.getFullContent());
//                        totalLOC += loc;
//                        content = content + loc + " ";
//                    }
//
//                    content = content + "</td>";
//
////                    NOM
//                    content = content + "<td>";
//
                    for (ClassModel m : entry.getValue()) {
                        Long nom = nomDetection.nomDetection(m);
                        totalNOM += nom;
//                        content = content + nom + " ";
                    }
//
//                    content = content + "</td>";
//
////                    NOF
//                    content = content + "<td>";
//
//                    for (ClassModel m : entry.getValue()) {
//                        Long nof = nofDetection.nofDetection(m);
//                        totalNOF += nof;
//                        content = content + nof + " ";
//                    }
//
//                    content = content + "</td>";

//                    DIT
                    content = content + "<td>";

                    for (ClassModel m : entry.getValue()) {
                        Long dit = ditDetection.DITDetection(m, classes);
                        if(dit > maxDIT) {
                            maxDIT = dit;
                        }
                        content = content + dit + " ";
                    }

                    content = content + "</td>";

//                    NOC
                    content = content + "<td>";

                    for (ClassModel m : entry.getValue()) {
                        Long noc = nocDetection.NOCDetection(m, classes);
                        totalNOC += noc;
                        content = content + noc + " ";
                    }

                    content = content + "</td>";

//                    LCOM
                    content = content + "<td>";

                    for (ClassModel m : entry.getValue()) {
                        Long lcom = lcomDetection.LCOMDetection(m);
                        totalLCOM += lcom;
                        content = content + lcom + " ";
                    }

                    content = content + "</td>";

//                    RFC
                    content = content + "<td>";

                    for (ClassModel m : entry.getValue()) {
                        Long rfc = rfcDetection.RFCDetection(m, classes);
                        totalRFC += rfc;
                        content = content + rfc + " ";
                    }

                    content = content + "</td>";

//                    WMC
                    content = content + "<td>";

                    for (ClassModel m : entry.getValue()) {
                        Long wmc = wmcDetection.WMCDetection(m);
                        totalWMC += wmc;
                        content = content + wmc + " ";
                    }

                    content = content + "</td>";

//                    CBO
                    content = content + "<td>";

                    for (ClassModel m : entry.getValue()) {
                        Long cbo = cboDetection.CBODetection(m, classes);
                        totalCBO += cbo;
                        content = content + cbo + " ";
                    }

                    content = content + "</td>";

//                    CC
                    content = content + "<td>";

                    for (ClassModel m : entry.getValue()) {
                        Long cc = ccDetection1.CCDetection(m);
                        totalCC += cc;
                        content = content + cc + " ";
                    }
                    float ccCalc = (float) totalCC / (float) totalNOM;

                    content = content + "</td>";

//                    CCC
                    content = content + "<td>";

                    for (ClassModel m : entry.getValue()) {
                        Long ccc = cccDetection.CCCDetection(m, classes);
                        totalCCC += ccc;
                        content = content + ccc + " ";
                    }

                    content = content + "</td>";


                    countClasses += entry.getValue().size();
                    found = true;
                }

            }

            if(!found) {
                content = content + "<td>-</td>";
                content = content + "<td>-</td>";
            }

            content = content + "</tr>";
            indexFile++;
        }

        content = content + "<tr>";
        content = content + "<th>Total</th>";
        content = content + "<th>" + (indexFile - 1) + "</th>";
        content = content + "<th>" + countClasses + "</th>";
        /*content = content + "<th>" + totalLOC + "</th>";
        content = content + "<th>" + totalNOM + "</th>";
        content = content + "<th>" + totalNOF + "</th>";*/
        content = content + "<th>" + maxDIT + "</th>";
        content = content + "<th>" + totalNOC + "</th>";
        content = content + "<th>" + totalLCOM + "</th>";
        content = content + "<th>" + totalRFC + "</th>";
        content = content + "<th>" + totalWMC + "</th>";
        content = content + "<th>" + totalCBO + "</th>";
        content = content + "<th>" + totalCC + "</th>";
        content = content + "<th>" + totalCCC + "</th>";
        content = content + "</tr>";

        content = content + "<tr>";
        content = content + "<th>Calculation</th>";
        content = content + "<th>" + "</th>";
        content = content + "<th>" +  "</th>";
       /* content = content + "<th>" + df.format((float)totalLOC/(float) countClasses) + "</th>";
        content = content + "<th>" + df.format((float)totalNOM/(float) countClasses) + "</th>";
        content = content + "<th>" + df.format((float)totalNOF/(float) countClasses) + "</th>";*/
        content = content + "<th>" + "</th>";
        content = content + "<th>" + df.format((float)totalNOC/(float) countClasses) + "</th>";
        content = content + "<th>" + df.format((float)totalLCOM/(float) countClasses) + "</th>";
        content = content + "<th>" + df.format((float)totalRFC/(float) countClasses) + "</th>";
        content = content + "<th>" + df.format((float)totalWMC/(float) countClasses)+ "</th>";
        content = content + "<th>" + df.format((float)totalCBO/(float) countClasses) + "</th>";
        content = content + "<th>" + df.format(1.0-(float)totalCC/(float) totalNOM) + "</th>";
        content = content + "<th>" + df.format((float)(countClasses - totalCCC)/(float) totalCCC) + "</th>";
        content = content + "</tr>";

        content = content + "</table>";

        return content;
    }
}
