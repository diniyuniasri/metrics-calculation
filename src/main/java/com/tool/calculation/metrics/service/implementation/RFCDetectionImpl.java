package com.tool.calculation.metrics.service.implementation;

import com.tool.calculation.metrics.model.ClassModel;
import com.tool.calculation.metrics.model.MethodModel;
import com.tool.calculation.metrics.service.RFCDetection;
import lombok.NonNull;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class RFCDetectionImpl implements RFCDetection {

    private String methodCallRegex = "(\\.[\\s\\n\\r]*[\\w]+)[\\s\\n\\r]*(?=\\(.*\\))";
    private String classVarCallRegex = "([\\w])*(?=(\\.[\\s\\n\\r]*[\\w]+)[\\s\\n\\r]*(?=\\(.*\\)))";
    private String classCallRegex1 = "([\\w])*(?=[\\s\\n\\r]+";

    @Override
    public Long RFCDetection(@NonNull ClassModel classModel, Map<String, List<ClassModel>> classes) {
        Long totalRFC = 0L;
        Set<String> methodCallNames = new HashSet<>();

        if (classModel.getMethodModels() != null) {

            totalRFC += classModel.getMethodModels().size();

            for (int i = 0; i < classModel.getMethodModels().size(); i++) {

//                System.out.println("Class Detection: " + classModel.getName());

                //find method' call
                MethodModel methodModel = classModel.getMethodModels().get(i);

//                System.out.println("Method Detection: " + methodModel.getName());

                Pattern methodPattern = Pattern.compile(methodCallRegex, Pattern.MULTILINE);
                Matcher methodMatcher = methodPattern.matcher(methodModel.getBody());

                while (methodMatcher.find()) {

                    //find the class variable' name
                    String classVarCallName = "";
                    for(int j=methodMatcher.start()-1; j >= 0; j--){
                        if(methodModel.getBody().charAt(j) == ' ') {
                            break;
                        }
                        classVarCallName += methodModel.getBody().charAt(j);
                    }

                    StringBuilder output = new StringBuilder(classVarCallName).reverse();
                    classVarCallName = output.toString();

                    if(!classVarCallName.matches("[a-zA-Z0-9]+")) {
                        continue;
                    }
//                    System.out.println(classVarCallName);
//
                        //find the class name
                        Pattern classPattern = Pattern.compile((classCallRegex1+classVarCallName+")"), Pattern.MULTILINE);
                        Matcher classMatcher = classPattern.matcher(classModel.getFullContent());

                        if (classMatcher.find()) {

                            String classCallName = classModel.getFullContent().substring(classMatcher.start(), classMatcher.end());

                            //compare the class name with the class inside the project

                            for (Map.Entry<String, List<ClassModel>> entry : classes.entrySet()) {
                                for(ClassModel model : entry.getValue()) {
                                    if(model.getName().equals(classCallName)) {
                                        String methodCallName = classVarCallName+methodModel.getBody().substring(methodMatcher.start(), methodMatcher.end());
//                                        System.out.println("verified1:" + methodCallName);
                                        methodCallNames.add(methodCallName);
                                    }
                                }

                            }
                        }
                };
            }

        }

//        System.out.println("size :" + methodCallNames.size());
//        System.out.println("========================================");
        return totalRFC + methodCallNames.size();
    }
}