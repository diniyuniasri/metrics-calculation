package com.tool.calculation.metrics.service.implementation;

import com.tool.calculation.metrics.model.*;
import com.tool.calculation.metrics.service.ClassAttributesAnalysis;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class ClassAttributesAnalysisImpl implements ClassAttributesAnalysis {

    @Value("${methods.detection.regex}")
    private String methodsRegex;

    @Override
    public void analysis(@NonNull FileModel fileModel, @NonNull ClassModel classModel, @NonNull ClassIndexModel indexModel) {

        Integer startIndex = indexModel.getEndDeclaration() + 1, endIndex = 0;

        Pattern methodPattern = Pattern.compile(methodsRegex, Pattern.MULTILINE);
        Matcher methodMatcher = methodPattern.matcher(fileModel.getContent());
        while (methodMatcher.find()) {
            endIndex = methodMatcher.start();
            break;
        }

        if (endIndex < startIndex) {
            endIndex = startIndex;
        }

        String rawAttributes = fileModel.getContent().substring(startIndex, endIndex).replace("\n", "").replace("\r", "").trim().replaceAll(" +", " ");
        String[] splitAttributes = rawAttributes.split(";");

        List<AttributeModel> attributes = new ArrayList<>();
        for(int i = 0; i < (splitAttributes.length - 1); i++){

            String attr = splitAttributes[i].trim();
//            System.out.println("raw split :" + attr);

            String[] splitMultiAttributes = attr.split(",");
            String type = "";

            for(int j = 0; j < splitMultiAttributes.length; j++){

                String attrMulti = splitMultiAttributes[j].trim();
//                System.out.println("raw multi split " + j + " :" + attrMulti);

                String name = attrMulti;
                String value = "";

                for (int k = 0; k < attrMulti.length(); k++) {
                    if(attrMulti.charAt(k) == '=') {
                        name = attrMulti.substring(0,k).trim();
                        value = attrMulti.substring(k+1).trim();
                        break;
                    }
                }

                if(j == 0) {
                    for (int l = (name.length() - 1); l >= 0; l--) {
                        if(name.charAt(l) == ' ') {
                            type = name.substring(0,l).trim();
                            name = name.substring(l+1).trim();
                            break;
                        }
                    }
                }

//                System.out.println(i + "\ntype: " + type + "\nname: " + name + "\nval: " + value);

                Pattern p = Pattern.compile("[^a-zA-Z0-9]");
                boolean hasSpecialChar = p.matcher(name).find();

                if(!hasSpecialChar) {
                    AttributeModel attributeModel = AttributeModel.builder()
                            .type(type)
                            .name(name)
                            .value(value)
                            .build();

                    attributes.add(attributeModel);
                }
            }
//

        }

//        System.out.println("--------------------------------------------------------------");

        classModel.setAttributes(attributes);
    }
}
