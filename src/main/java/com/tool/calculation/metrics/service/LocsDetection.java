package com.tool.calculation.metrics.service;

import lombok.NonNull;

/**
 * @author M. Dini Yuniasri
 * @version 1.0.0
 * @since 27 April 2019
 */

public interface LocsDetection {

    Long locDetection(@NonNull String body);
}