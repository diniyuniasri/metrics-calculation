package com.tool.calculation.metrics.service;

import com.tool.calculation.metrics.model.ClassModel;
import lombok.NonNull;

public interface NOMDetection {

    Long nomDetection(@NonNull ClassModel classModel);
}