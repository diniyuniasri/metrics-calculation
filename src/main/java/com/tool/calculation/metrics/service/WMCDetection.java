package com.tool.calculation.metrics.service;

import com.tool.calculation.metrics.model.ClassModel;
import lombok.NonNull;

public interface WMCDetection {
    Long WMCDetection(@NonNull ClassModel classModel);
}
