package com.tool.calculation.metrics.service.implementation;

import com.tool.calculation.metrics.model.IndexModel;
import com.tool.calculation.metrics.model.MethodModel;
import com.tool.calculation.metrics.service.MethodBodyAnalysis;
import lombok.NonNull;
import org.springframework.stereotype.Service;

import java.util.Stack;

/**
 * @author M. Dini Yuniasri
 * @version 1.0.0
 * @since 27 April 2019
 */

@Service
public class MethodBodyAnalysisImpl implements MethodBodyAnalysis {

    private static final String OPEN_BRACES = "{";
    private static final String CLOSE_BRACES = "}";

    @Override
    public void analysis(@NonNull String content, @NonNull IndexModel indexModel,
                         @NonNull MethodModel methodModel) {
        Integer startBodyIndex = indexModel.getStart();

        String body = searchMethodBody(content, startBodyIndex, indexModel.getEnd());
        methodModel.setBody(body);
    }

    private String searchMethodBody(String content, Integer startBodyIndex, Integer endIndex) {
        Stack<Integer> stack = new Stack<>();
        Integer index;

        for (index = startBodyIndex; index < content.length(); index++) {
            String character = String.valueOf(content.charAt(index));

            switch (character) {
                case OPEN_BRACES:
                    stack.push(index);
                    break;
                case CLOSE_BRACES:
                    startBodyIndex = stack.pop();
                    break;
            }

            if (index > endIndex && stack.empty())
                break;
        }

        return content.substring(startBodyIndex + 1, index - 1);
    }
}
