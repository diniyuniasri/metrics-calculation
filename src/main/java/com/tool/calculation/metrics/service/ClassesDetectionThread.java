package com.tool.calculation.metrics.service;

import com.tool.calculation.metrics.model.ClassModel;
import com.tool.calculation.metrics.model.FileModel;
import lombok.NonNull;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

public interface ClassesDetectionThread {
    Future detect(@NonNull FileModel fileModel, @NonNull Map<String, List<ClassModel>> result);
}
