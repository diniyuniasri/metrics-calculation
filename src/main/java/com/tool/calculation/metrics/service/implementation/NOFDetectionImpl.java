package com.tool.calculation.metrics.service.implementation;

import com.tool.calculation.metrics.model.ClassModel;
import com.tool.calculation.metrics.service.NOFDetection;
import lombok.NonNull;
import org.springframework.stereotype.Service;

@Service
public class NOFDetectionImpl implements NOFDetection {

    @Override
    public Long nofDetection(@NonNull ClassModel classModel) {
        if(classModel.getAttributes() == null){
            return 0L;
        }
        else{
            return new Long(classModel.getAttributes().size());
        }
    }
}