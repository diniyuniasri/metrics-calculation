package com.tool.calculation.metrics.service;

import com.tool.calculation.metrics.model.FileModel;
import lombok.NonNull;

import java.util.List;
import java.util.Map;

/**
 * @author M. Dini Yuniasri
 * @version 1.0.0
 * @since 27 April 2019
 */

public interface FilesDetectionThread {

    void detect(@NonNull String path, @NonNull String mimeType,
                @NonNull Map<String, List<FileModel>> result);
}
