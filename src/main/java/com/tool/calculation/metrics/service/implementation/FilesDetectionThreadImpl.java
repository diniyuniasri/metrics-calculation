package com.tool.calculation.metrics.service.implementation;

import com.tool.calculation.metrics.model.FileModel;
import com.tool.calculation.metrics.service.FilesDetectionThread;
import lombok.NonNull;
import org.apache.tika.Tika;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @author M. Dini Yuniasri
 * @version 1.0.0
 * @since 27 April 2019
 */

@Service
public class FilesDetectionThreadImpl implements FilesDetectionThread {

    private static final String NEW_LINE_DELIMITER = "\n";
    private String commentRegex = "//.*|(\"(?:\\\\[^\"]|\\\\\"|.)*?\")|(?s)/\\*.*?\\*/";


    @Override
    public void detect(@NonNull String path, @NonNull String mimeType,
                       @NonNull Map<String, List<FileModel>> result) {
        List<FileModel> files = readFiles(path, mimeType);
        result.put(path, files);
    }

    private List<FileModel> readFiles(String path, String mimeType) {
        List<FileModel> result = null;
        Path start = Paths.get(path);

        try {
            result = Files.walk(start)
                    .filter(filePath -> checkType(filePath, mimeType))
                    .map(this::mapIntoFileModel)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            printPathError(path);
        }

        return result;
    }

    private Boolean checkType(Path filePath, String mimeType) {
        Tika tika = new Tika();

        try {
            return Optional.of(tika.detect(filePath))
                    .map(type -> isTypeEquals(type, mimeType))
                    .get();
        } catch (IOException | NullPointerException e) {
            return Boolean.FALSE;
        }
    }

    private Boolean isTypeEquals(String type, String mimeType) {
        return type.equals(mimeType);
    }

    private FileModel mapIntoFileModel(Path filePath) {
        return FileModel.builder()
                .path(filePath.getParent().toString())
                .fullPath(filePath.toString())
                .content(getFileContent(filePath))
                .filename(filePath.getFileName().toString())
                .build();
    }

    private String getFileContent(Path filePath) {
        String result = null;

        try {
            result = Files.lines(filePath)
                    .collect(Collectors.joining(NEW_LINE_DELIMITER));
        } catch (IOException e) {
            printPathError(filePath.toString());
        }

        String clean = result.replaceAll( commentRegex, "$1 " );

        return clean;
    }

    private void printPathError(String path) {
        System.err.println("Error opening " + path + "...");
    }
}
