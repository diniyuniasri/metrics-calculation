package com.tool.calculation.metrics.service;

import com.tool.calculation.metrics.model.ClassModel;
import lombok.NonNull;

import java.util.List;
import java.util.Map;

public interface DITDetection {
    Long DITDetection(@NonNull ClassModel classModel, Map<String, List<ClassModel>> classes);
}