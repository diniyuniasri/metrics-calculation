package com.tool.calculation.metrics.service.implementation;

import com.tool.calculation.metrics.model.FileModel;
import com.tool.calculation.metrics.service.MethodsDetectionUtil;
import lombok.NonNull;
import org.springframework.stereotype.Service;

import java.io.File;

/**
 * @author M. Dini Yuniasri
 * @version 1.0.0
 * @since 27 April 2019
 */

@Service
public class MethodsDetectionUtilImpl implements MethodsDetectionUtil {

    @Override
    public String getMethodKey(@NonNull FileModel fileModel) {
        return fileModel.getPath() + File.separator + fileModel.getFilename();
    }
}
