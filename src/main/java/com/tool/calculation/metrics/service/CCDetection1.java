package com.tool.calculation.metrics.service;

import com.tool.calculation.metrics.model.ClassModel;
import lombok.NonNull;

public interface CCDetection1 {
    Long CCDetection(@NonNull ClassModel classModel);
}
