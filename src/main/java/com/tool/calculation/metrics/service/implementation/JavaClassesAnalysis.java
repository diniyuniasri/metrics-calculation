package com.tool.calculation.metrics.service.implementation;

import com.tool.calculation.metrics.model.ClassIndexModel;
import com.tool.calculation.metrics.model.ClassModel;
import com.tool.calculation.metrics.model.FileModel;
import com.tool.calculation.metrics.model.IndexModel;
import com.tool.calculation.metrics.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

@Service
public class JavaClassesAnalysis implements ClassAnalysis {

    @Autowired
    private ClassesDetectionUtil classesDetectionUtil;

    @Autowired
    private ClassMethodAnalysis classMethodAnalysis;

    @Autowired
    private ClassNameAnalysis classNameAnalysis;

    @Autowired
    private ClassPackageImportAnalysis classPackageImportAnalysis;

    @Autowired
    private ClassAttributesAnalysis classAttributesAnalysis;

    @Async
    @Override
    public Future analysis(FileModel fileModel, ClassIndexModel indexModel,
                           Map<String, List<ClassModel>> result) {

        System.out.println(fileModel.getFilename() + " " + indexModel.getStartDeclaration() + ", " + indexModel.getEndDeclaration() + ", " + indexModel.getEndClass());
        System.out.println(fileModel.getContent().substring(indexModel.getStartDeclaration(), indexModel.getEndDeclaration() + 1) + "..." + fileModel.getContent().charAt(indexModel.getEndClass()));
        System.out.println(fileModel.getFullPath());
//        System.out.println("--------------------------------------------------------------");

        String classbody = fileModel.getContent().substring(indexModel.getEndDeclaration(), indexModel.getEndClass() + 1);
        ClassModel classModel = ClassModel.builder().build();
        try {
            classPackageImportAnalysis.analysis(fileModel, classModel);
            classNameAnalysis.analysis(fileModel, classModel, indexModel);
            classMethodAnalysis.analysis(fileModel, classModel, indexModel);
            classAttributesAnalysis.analysis(fileModel, classModel, indexModel);
            classModel.setFullContent(classbody);
            classModel.setPath(fileModel.getPath());
            classModel.setFullPath(fileModel.getFullPath());
            saveResult(fileModel, classModel, result);
        } catch (Exception e) {
            // Do nothing
            // Mark of non-class analysis
        }
        return null;
    }

    private void saveResult(FileModel fileModel, ClassModel classModel,
                            Map<String, List<ClassModel>> result) {
        String key = classesDetectionUtil.getClassKey(fileModel);

        if (result.containsKey(key))
            result.get(key).add(classModel);
        else {
            List<ClassModel> classModels = new ArrayList<>();
            classModels.add(classModel);

            result.put(key, classModels);
        }
    }
}
