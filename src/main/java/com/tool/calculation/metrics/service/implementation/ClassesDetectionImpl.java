package com.tool.calculation.metrics.service.implementation;

import com.tool.calculation.metrics.model.ClassModel;
import com.tool.calculation.metrics.model.FileModel;
import com.tool.calculation.metrics.service.ClassesDetection;
import com.tool.calculation.metrics.service.ClassesDetectionThread;
import com.tool.calculation.metrics.service.ClassesDetectionUtil;
import com.tool.calculation.metrics.service.ThreadsWatcher;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

@Service
public class ClassesDetectionImpl implements ClassesDetection {

    @Autowired
    private ClassesDetectionThread classesDetectionThread;

    @Autowired
    private ThreadsWatcher threadsWatcher;

    @Autowired
    private ClassesDetectionUtil classesDetectionUtil;

    @Value("${threads.waiting.time}")
    private Integer waitingTime;

    @Override
    public List<ClassModel> detect(@NonNull FileModel fileModel) {
        return detect(Collections.singletonList(fileModel))
                .get(classesDetectionUtil.getClassKey(fileModel));
    }

    @Override
    public Map<String, List<ClassModel>> detect(@NonNull List<FileModel> fileModels) {
        Map<String, List<ClassModel>> result = Collections.synchronizedMap(new HashMap<>());
        List<Future> threads = doClassesDetection(fileModels, result);

        threadsWatcher.waitAllThreadsDone(threads, waitingTime);

        return result;
    }

    private List<Future> doClassesDetection(List<FileModel> fileModels, Map<String, List<ClassModel>> result) {
        return fileModels.stream()
                .map(fileModel -> classesDetectionThread.detect(fileModel, result))
                .collect(Collectors.toList());
    }
}
