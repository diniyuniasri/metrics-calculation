package com.tool.calculation.metrics.service.implementation;

import com.tool.calculation.metrics.model.FileModel;
import com.tool.calculation.metrics.service.ClassesDetectionUtil;
import lombok.NonNull;
import org.springframework.stereotype.Service;

import java.io.File;

@Service
public class ClassesDetectionUtilImpl implements ClassesDetectionUtil {
    
    @Override
    public String getClassKey(@NonNull FileModel fileModel) {
        return fileModel.getPath() + File.separator + fileModel.getFilename();
    }
}
