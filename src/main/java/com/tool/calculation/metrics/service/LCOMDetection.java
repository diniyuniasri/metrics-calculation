package com.tool.calculation.metrics.service;

import com.tool.calculation.metrics.model.ClassModel;
import lombok.NonNull;

public interface LCOMDetection {
    Long LCOMDetection(@NonNull ClassModel classModel);
}
