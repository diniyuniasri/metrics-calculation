package com.tool.calculation.metrics.service.implementation;

import com.tool.calculation.metrics.model.ClassModel;
import com.tool.calculation.metrics.service.DITDetection;
import lombok.NonNull;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class DITDetectionImpl implements DITDetection {

    @Override
    public Long DITDetection(@NonNull ClassModel classModel, @NonNull Map<String, List<ClassModel>> classes) {
        Long totalDIT = 1L;

        totalDIT = checkDeepInheritance(classModel, classes, totalDIT);

        return totalDIT;
    }

    public Long checkDeepInheritance(ClassModel classModel, Map<String, List<ClassModel>> classes, Long count) {
        Long dit = count;

        if(classModel.getExtend() != null) {

            String parent = classModel.getExtend();
            ClassModel parentModel = null;

            boolean found = false;

            for (Map.Entry<String, List<ClassModel>> entry : classes.entrySet()) {
                for(ClassModel model : entry.getValue()) {
                    if(model.getName().equals(parent)) {
                        found = true;
                        parentModel = model;
                        break;
                    }
                }
                if(found){
                    break;
                }

            }

            if(found){
                dit += 1;
                return checkDeepInheritance(parentModel, classes, dit);
            }
            else {
                return dit;
            }

        }

        else {
            return dit;
        }

    }
}