package com.tool.calculation.metrics.service;

import com.tool.calculation.metrics.model.IsStatementVA;
import lombok.NonNull;

/**
 * @author M. Dini Yuniasri
 * @version 1.0.0
 * @since 27 April 2019
 */

public interface StatementHelper {

    Boolean isStatement(@NonNull Character character, @NonNull IsStatementVA isStatementVA);
}
