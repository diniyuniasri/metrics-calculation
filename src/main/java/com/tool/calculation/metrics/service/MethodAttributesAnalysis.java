package com.tool.calculation.metrics.service;

import com.tool.calculation.metrics.model.FileModel;
import com.tool.calculation.metrics.model.IndexModel;
import com.tool.calculation.metrics.model.MethodModel;
import lombok.NonNull;

/**
 * @author M. Dini Yuniasri
 * @version 1.0.0
 * @since 27 April 2019
 */

public interface MethodAttributesAnalysis {

    void analysis(@NonNull FileModel fileModel, @NonNull String classBody, @NonNull IndexModel indexModel,
                  @NonNull MethodModel methodModel);
}
