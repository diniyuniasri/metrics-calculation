package com.tool.calculation.metrics;

import com.tool.calculation.metrics.service.Main;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.List;

@SpringBootApplication
public class Demo extends JFrame {
    private JTextField txtPath;
    private JButton submitButton;
    private JPanel rootPanel;
    private JEditorPane editorPane1;
    private static ConfigurableApplicationContext applicationContext;
    String path;

    public static void main(String[] args) {
        applicationContext = new SpringApplicationBuilder(Demo.class)
                .headless(false).run(args);

        SwingUtilities.invokeLater(() ->  {
            applicationContext.getBean(Demo.class).setLocationRelativeTo(null);
            applicationContext.getBean(Demo.class).setVisible(true);
        });
    }

    public Demo() {
        add(rootPanel);
        setTitle("Metrics Calculation");
        setSize(800, 650);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        editorPane1.setEditable(false);
        txtPath.setText("D:\\# thesis\\dataset\\4 DataMining-master");

        submitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                path = txtPath.getText();
                String result = applicationContext.getBean(Main.class).doMetricsCalculation(Arrays.asList(path));
                editorPane1.setText(result);
            }
        });
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }
}
